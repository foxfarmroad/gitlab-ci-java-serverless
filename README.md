# gitlab-ci-java-serverless
## Docker image to build and deploy Java-based Serverless applications
## Builds
[Automated builds](https://hub.docker.com/r/foxfarmroad/gitlab-ci-java-serverless/builds/) set up on [Docker Hub](https://hub.docker.com)